// Project1.cpp : Defines the entry point for the application.
//
#include "stdafx.h"
#include "Project1.h"
#include <string>
#include <commctrl.h>	
#include <commdlg.h>
#define MAX_LOADSTRING 100
#define MAX_LOADSTRING	100
#define ID_TOOLBAR		1000	// ID of the toolbar
#define IMAGE_WIDTH     18
#define IMAGE_HEIGHT    17
#define BUTTON_WIDTH    0
#define BUTTON_HEIGHT   0
#define TOOL_TIP_MAX_LEN   32

// Global Variables:
HINSTANCE hInst;                                // current instance
WCHAR szTitle[MAX_LOADSTRING];                  // The title bar text
WCHAR szWindowClass[MAX_LOADSTRING];            // the main window class name
WCHAR BITMAPCLASS[MAX_LOADSTRING];
HWND  hToolBarWnd;								// Handle of the ToolBar

// Forward declarations of functions included in this code module:
ATOM                MyRegisterClass(HINSTANCE hInstance);
BOOL                InitInstance(HINSTANCE, int);
LRESULT CALLBACK    WndProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK    About(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK    Color(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam);

// MDI CHILD TEXT EDIT
LRESULT CALLBACK    BitProc(HWND, UINT, WPARAM, LPARAM);

//ENUM
BOOL CALLBACK CloseEnumProc(HWND, LPARAM);

// toolbar functions
void doCreate_ToolBar(HWND hWnd);

void OnLButtonDown(LPARAM lParam, int &x1, int &y1, int &x2, int &y2);
void OnMouseMove(HWND hWnd, WPARAM wParam, LPARAM lParam, int x1, int y1, int &x2, int &y2,int type);
// GLOBAL VAR
HWND hwndMDIClient;
HMENU FrameMenu;
int bcount = 1;
int type=-1;
int APIENTRY wWinMain(_In_ HINSTANCE hInstance,
                     _In_opt_ HINSTANCE hPrevInstance,
                     _In_ LPWSTR    lpCmdLine,
                     _In_ int       nCmdShow)
{
    UNREFERENCED_PARAMETER(hPrevInstance);
    UNREFERENCED_PARAMETER(lpCmdLine);

    // TODO: Place code here.

    // Initialize global strings
    LoadStringW(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
    LoadStringW(hInstance, IDC_PROJECT1, szWindowClass, MAX_LOADSTRING);
	LoadStringW(hInstance, IDS_BITMAP, BITMAPCLASS, MAX_LOADSTRING);
    MyRegisterClass(hInstance);

    // Perform application initialization:
    if (!InitInstance (hInstance, nCmdShow))
    {
        return FALSE;
    }

    HACCEL hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_PROJECT1));

    MSG msg;

    // Main message loop:
    while (GetMessage(&msg, nullptr, 0, 0))
    {
		if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg) && !TranslateMDISysAccel(hwndMDIClient, &msg))
        {
            TranslateMessage(&msg);
            DispatchMessage(&msg);
        }
    }

    return (int) msg.wParam;
}



//
//  FUNCTION: MyRegisterClass()
//
//  PURPOSE: Registers the window class.
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
    WNDCLASSEXW wcex;

    wcex.cbSize = sizeof(WNDCLASSEX);

	wcex.style = CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc = WndProc;
	wcex.cbClsExtra = 0;
	wcex.cbWndExtra = 0;
	wcex.hInstance = hInstance;
	wcex.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_PROJECT1));
	wcex.hCursor = LoadCursor(nullptr, IDC_ARROW);
	wcex.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);
	wcex.lpszMenuName = MAKEINTRESOURCEW(IDC_PROJECT1);
	wcex.lpszClassName = szWindowClass;
	wcex.hIconSm = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));
	RegisterClassExW(&wcex);


	wcex.cbSize = sizeof(WNDCLASSEX);
	wcex.lpfnWndProc = BitProc;
	wcex.hInstance = hInstance;
	wcex.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_PROJECT1));
	wcex.hCursor = LoadCursor(nullptr, IDC_ARROW);
	wcex.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);
	wcex.lpszMenuName = NULL;
	wcex.lpszClassName = BITMAPCLASS;
	RegisterClassExW(&wcex);
	return true;
}

//
//   FUNCTION: InitInstance(HINSTANCE, int)
//
//   PURPOSE: Saves instance handle and creates main window
//
//   COMMENTS:
//
//        In this function, we save the instance handle in a global variable and
//        create and display the main program window.
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
   hInst = hInstance; // Store instance handle in our global variable

   HWND hWnd = CreateWindowW(szWindowClass, szTitle, WS_OVERLAPPEDWINDOW,
      CW_USEDEFAULT, 0, CW_USEDEFAULT, 0, nullptr, nullptr, hInstance, nullptr);

   if (!hWnd)
   {
      return FALSE;
   }

   ShowWindow(hWnd, nCmdShow);
   UpdateWindow(hWnd);

   return TRUE;
}

//
//  FUNCTION: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  PURPOSE:  Processes messages for the main window.
//
//  WM_COMMAND  - process the application menu
//  WM_PAINT    - Paint the main window
//  WM_DESTROY  - post a quit message and return
//
//
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	FrameMenu = GetMenu(hWnd);
    switch (message)
    {
    case WM_COMMAND:
        {
            int wmId = LOWORD(wParam);
            // Parse the menu selections:
            switch (wmId)
            {
            case IDM_ABOUT:
                DialogBox(hInst, MAKEINTRESOURCE(IDD_ABOUTBOX), hWnd, About);
                break;
            case IDM_EXIT:
                DestroyWindow(hWnd);
                break;
			case ID_FILE_NEW:
			{

				MDICREATESTRUCT mdiCreate;
				mdiCreate.szClass = BITMAPCLASS;
				std::wstring ws(L"NoName");
				std::string title(ws.begin(), ws.end());
				title.append("-");
				title.append(std::to_string(bcount));
				std::wstring fulltitle(title.begin(), title.end());
				mdiCreate.szTitle = fulltitle.c_str();;
				mdiCreate.hOwner = hInst;
				mdiCreate.x = CW_USEDEFAULT;
				mdiCreate.y = CW_USEDEFAULT;
				mdiCreate.cx = CW_USEDEFAULT;
				mdiCreate.cy = CW_USEDEFAULT;
				mdiCreate.style = 0;
				mdiCreate.lParam = NULL;
				SendMessage(hwndMDIClient, WM_MDICREATE, 0, (LPARAM)(LPMDICREATESTRUCT)&mdiCreate);
				bcount++;
			}
			break;
			case ID_FILE_OPEN:
			{
				/*MessageBox(NULL, L"Ban da chon Open", L"Open", MB_OK);*/
				OPENFILENAME ofn;
				TCHAR szFile[256];
				TCHAR szFilter[] = TEXT("Word file\0 * .doc\0Excel file\0 * .xls\0Text file\0 * .txt\0");
				szFile[0] = '\0';
				ZeroMemory(&ofn, sizeof(OPENFILENAME));
				ofn.lStructSize = sizeof(OPENFILENAME);
				ofn.hwndOwner = hWnd;
				ofn.lpstrFilter = szFilter;
				ofn.nFilterIndex = 1;
				ofn.lpstrFile = szFile;
				ofn.nMaxFile = sizeof(szFile);
				ofn.Flags = OFN_PATHMUSTEXIST | OFN_FILEMUSTEXIST;
				if (GetOpenFileName(&ofn))
				{
					//xu ly mo file
				}
				break;
			}
			case ID_FILE_SAVE:
				/*MessageBox(NULL,L"Ban da chon Save",L"Save", MB_OK);*/
				break;
			case ID_WINDOW_TILE:
				SendMessage(hwndMDIClient, WM_MDITILE, MDITILE_VERTICAL, 0);
				break;
			case ID_WINDOW_CLOSEALL:
				EnumChildWindows(hwndMDIClient, CloseEnumProc, 0);
				break;
			case ID_WINDOW_CASCADE:
				SendMessage(hwndMDIClient, WM_MDICASCADE, MDITILE_SKIPDISABLED, 0);
				break;
			case ID_DRAW_COLOR:
			{
				/*DialogBox(hInst, MAKEINTRESOURCE(IDD_COLOR), hWnd, Color);*/
				CHOOSECOLOR cc;
				COLORREF acrCustClr[16];
				WORD rgbCurrent = RGB(255, 0, 0);
				ZeroMemory(&cc, sizeof(CHOOSECOLOR));
				cc.lStructSize = sizeof(CHOOSECOLOR);
				cc.hwndOwner = hWnd;
				cc.lpCustColors = (LPDWORD)acrCustClr;
				cc.rgbResult = rgbCurrent;
				cc.Flags = CC_FULLOPEN | CC_RGBINIT;
				if (ChooseColor(&cc))
				{
					HBRUSH hbrush;
					hbrush = CreateSolidBrush(cc.rgbResult);
					rgbCurrent = cc.rgbResult;
				}
				break;
			}
			case ID_DRAW_FONT:
			{
				CHOOSEFONT cf;
				LOGFONT lf;
				HFONT hfNew, hfOld;
				ZeroMemory(&cf, sizeof(CHOOSEFONT));
				cf.lStructSize = sizeof(CHOOSEFONT);
				cf.hwndOwner = hWnd;
				cf.lpLogFont = &lf;
				cf.Flags = CF_SCREENFONTS | CF_EFFECTS;
				if (ChooseFont(&cf))
				{/*
					hfNew = CreateFontIndirect(cf.lpLogFont);  
					hfOld = SelectObject(hdc, hfNew);  
					rgbPrev = SetTextColor(hdc, cf.rgbColors);*/
				}
				break;
			}
			case ID_DRAW_ELLIPSE:
			{
				
				if (GetMenuState(FrameMenu, ID_DRAW_ELLIPSE, MF_BYCOMMAND) == MF_UNCHECKED)
				{
					type = 2;
					CheckMenuItem(FrameMenu, ID_DRAW_ELLIPSE, MF_CHECKED);
					CheckMenuItem(FrameMenu, ID_DRAW_RECTANGLE, MF_UNCHECKED);
					CheckMenuItem(FrameMenu, ID_DRAW_LINE, MF_UNCHECKED);
				}
			}
			break;
			case ID_DRAW_RECTANGLE:
			{
				type = 1;
				if (GetMenuState(FrameMenu, ID_DRAW_RECTANGLE, MF_BYCOMMAND) == MF_UNCHECKED)
				{
					CheckMenuItem(FrameMenu, ID_DRAW_RECTANGLE, MF_CHECKED);
					CheckMenuItem(FrameMenu, ID_DRAW_ELLIPSE, MF_UNCHECKED);
					CheckMenuItem(FrameMenu, ID_DRAW_LINE, MF_UNCHECKED);
				}
			}
			break;
			case ID_DRAW_LINE:
			{
				type = 0;
				if (GetMenuState(FrameMenu, ID_DRAW_LINE, MF_BYCOMMAND) == MF_UNCHECKED)
				{
					CheckMenuItem(FrameMenu, ID_DRAW_LINE, MF_CHECKED);
					CheckMenuItem(FrameMenu, ID_DRAW_RECTANGLE, MF_UNCHECKED);
					CheckMenuItem(FrameMenu, ID_DRAW_ELLIPSE, MF_UNCHECKED);
				}
			}
			break;
            default:
				return DefFrameProc(hWnd, hwndMDIClient, message, wParam, lParam);
            }
			return DefFrameProc(hWnd, hwndMDIClient, message, wParam, lParam);
        }
        break;
    case WM_PAINT:
        {
            PAINTSTRUCT ps;
            HDC hdc = BeginPaint(hWnd, &ps);
            // TODO: Add any drawing code that uses hdc here...
            EndPaint(hWnd, &ps);
        }
        break;
	case WM_CREATE:
		doCreate_ToolBar(hWnd);
		CLIENTCREATESTRUCT ccs;
		ccs.hWindowMenu = GetSubMenu(GetMenu(hWnd), 4);
		ccs.idFirstChild = 50001;
		hwndMDIClient = CreateWindow(TEXT("MDICLIENT"), (LPCTSTR)NULL, WS_CHILD | WS_CLIPCHILDREN | WS_VSCROLL | WS_HSCROLL, 0, 0, 0, 0, hWnd, (HMENU)NULL, hInst, (LPVOID)&ccs);
		ShowWindow(hwndMDIClient, SW_SHOW);
		return 0;
	case WM_SIZE:
		UINT w, h;
		w = LOWORD(lParam);
		h = HIWORD(lParam);
		MoveWindow(hwndMDIClient, 0,30, w, h, TRUE);
		return 0;
    case WM_DESTROY:
        PostQuitMessage(0);
        break;
    default:
		return DefFrameProc(hWnd, hwndMDIClient, message, wParam, lParam);
    }
	return DefFrameProc(hWnd, hwndMDIClient, message, wParam, lParam);
}

// Message handler for about box.
INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
    UNREFERENCED_PARAMETER(lParam);
    switch (message)
    {
    case WM_INITDIALOG:
        return (INT_PTR)TRUE;

    case WM_COMMAND:
        if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
        {
            EndDialog(hDlg, LOWORD(wParam));
            return (INT_PTR)TRUE;
        }
        break;
    }
    return (INT_PTR)FALSE;
}
INT_PTR CALLBACK Color(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	UNREFERENCED_PARAMETER(lParam);
	switch (message)
	{
	case WM_INITDIALOG:
		return (INT_PTR)TRUE;

	case WM_COMMAND:
		if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
		{
			EndDialog(hDlg, LOWORD(wParam));
			return (INT_PTR)TRUE;
		}
		break;
	}
	return (INT_PTR)FALSE;
}
struct LINE
{
	int x1, y1, x2, y2;
};
LINE Line[100];
int nCurrentLine = 0;
int x1, y, x2, y2;
LRESULT CALLBACK BitProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	static HWND hwndEdit;
	switch (message)
	{
	case WM_CREATE:
		hInst = ((LPCREATESTRUCT)lParam)->hInstance;
		hwndEdit = CreateWindow(TEXT("bitmap"), NULL, WS_CHILD | WS_VISIBLE | WS_VSCROLL | WS_BORDER | ES_LEFT | ES_MULTILINE | ES_NOHIDESEL | ES_AUTOHSCROLL, 0, 0, 0, 0, hWnd, 0, hInst, NULL);
		return DefMDIChildProc(hWnd, message, wParam, lParam);
	case WM_SETFOCUS:
		SetFocus(hwndEdit);
		return DefMDIChildProc(hWnd, message, wParam, lParam);
	case WM_SIZE:
		MoveWindow(hwndEdit,0, 0, LOWORD(lParam), HIWORD(lParam), TRUE);
		return DefMDIChildProc(hWnd, message, wParam, lParam);
	case WM_MDIACTIVATE:
		EnableMenuItem(FrameMenu, ID_DRAW_ELLIPSE, MF_ENABLED);
		EnableMenuItem(FrameMenu, ID_DRAW_LINE, MF_ENABLED);
		EnableMenuItem(FrameMenu, ID_DRAW_RECTANGLE, MF_ENABLED);
		return DefMDIChildProc(hWnd, message, wParam, lParam);
	case WM_LBUTTONDOWN:
	{
		OnLButtonDown(lParam, x1, y, x2, y2);
		break;
	}
	case WM_MOUSEMOVE:
	{
		OnMouseMove(hWnd, wParam, lParam, x1, y, x2, y2,type);
		break;
	}
	case WM_LBUTTONUP:
	{
		Line[nCurrentLine].x1 = x1;
		Line[nCurrentLine].y1 = y;
		Line[nCurrentLine].x2 = x2;
		Line[nCurrentLine].y2 = y2;
		nCurrentLine++;
		break;
	}
	case WM_PAINT:
	{
		PAINTSTRUCT ps;
		HDC hdc = BeginPaint(hWnd, &ps);
		for (int i = 0; i < nCurrentLine; i++)
		{
			HDC dc = GetDC(hWnd);
			MoveToEx(dc, Line[i].x1, Line[i].y1, NULL);
			LineTo(dc, Line[i].x2, Line[i].y2);
			ReleaseDC(hWnd, dc);
		}
		EndPaint(hWnd, &ps);
	}
	default:
		break;
	}
	return DefMDIChildProc(hWnd, message, wParam, lParam);
}

BOOL CALLBACK CloseEnumProc(HWND hwnd, LPARAM lParam)
{
	if (GetWindow(hwnd, GW_OWNER)) // Check for icon title
		return TRUE;
	SendMessage(GetParent(hwnd), WM_MDIRESTORE, (WPARAM)hwnd, 0);
	if (!SendMessage(hwnd, WM_QUERYENDSESSION, 0, 0))
		return TRUE;
	SendMessage(GetParent(hwnd), WM_MDIDESTROY, (WPARAM)hwnd, 0);
	return TRUE;
}
void doCreate_ToolBar(HWND hWnd)
{
	// loading Common Control DLL
	InitCommonControls();

	TBBUTTON tbButtons[] =
	{
		// Zero-based Bitmap image, ID of command, Button state, Button style, 
		// ...App data, Zero-based string (Button's label)
		{ STD_FILENEW,	ID_FILE_NEW, TBSTATE_ENABLED, TBSTYLE_BUTTON, 0, 0 },
		{ STD_FILEOPEN,	ID_FILE_OPEN, TBSTATE_ENABLED, TBSTYLE_BUTTON, 0, 0 },
		{ STD_FILESAVE,	ID_FILE_SAVE, TBSTATE_ENABLED, TBSTYLE_BUTTON, 0, 0 }
	};
	// create a toolbar
	hToolBarWnd = CreateToolbarEx(hWnd,
		WS_CHILD | WS_VISIBLE | CCS_ADJUSTABLE | TBSTYLE_TOOLTIPS,
		ID_TOOLBAR,
		sizeof(tbButtons) / sizeof(TBBUTTON),
		HINST_COMMCTRL,
		0,
		tbButtons,
		sizeof(tbButtons) / sizeof(TBBUTTON),
		BUTTON_WIDTH,
		BUTTON_HEIGHT,
		IMAGE_WIDTH,
		IMAGE_HEIGHT,
		sizeof(TBBUTTON));

	// define new buttons
	TBBUTTON tbnewButtons[] =
	{
		{ 0, 0,	TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0 },
		{ 0, ID_DRAW_LINE,	TBSTATE_ENABLED, TBSTYLE_BUTTON, 0, 0 },
		{ 1,  ID_DRAW_RECTANGLE,	TBSTATE_ENABLED, TBSTYLE_BUTTON, 0, 0 },
		{ 2, ID_DRAW_ELLIPSE,	TBSTATE_ENABLED, TBSTYLE_BUTTON, 0, 0 },
		{ 3,  ID_DRAW_TEXT,	TBSTATE_ENABLED, TBSTYLE_BUTTON, 0, 0 }
	};

	// structure contains the bitmap of user defined buttons. It contains 2 icons
	TBADDBITMAP	tbBitmap = { hInst, IDB_DRAW};

	// Add bitmap to Image-list of ToolBar
	int idx = SendMessage(hToolBarWnd, TB_ADDBITMAP, (WPARAM) sizeof(tbBitmap) / sizeof(TBADDBITMAP),
		(LPARAM)(LPTBADDBITMAP)&tbBitmap);

	// identify the bitmap index of each button
	tbnewButtons[1].iBitmap += idx;
	tbnewButtons[2].iBitmap += idx;
	tbnewButtons[3].iBitmap += idx;
	tbnewButtons[4].iBitmap += idx;
	// add buttons to toolbar
	SendMessage(hToolBarWnd, TB_ADDBUTTONS, (WPARAM) sizeof(tbnewButtons) / sizeof(TBBUTTON),
		(LPARAM)(LPTBBUTTON)&tbnewButtons);
}
void OnLButtonDown(LPARAM lParam, int &x1, int &y1, int &x2, int &y2)
{
	x1 = x2 = LOWORD(lParam);
	y1 = y2 = HIWORD(lParam);
}
void OnMouseMove(HWND hWnd, WPARAM wParam, LPARAM lParam, int x1, int y1, int &x2, int &y2,int type)
{
	if (type == 0)
	{
		if (!(wParam & MK_LBUTTON)) return;
		HDC dc = GetDC(hWnd);

		SetROP2(dc, R2_NOTXORPEN);
		MoveToEx(dc, x1, y1, NULL);
		LineTo(dc, x2, y2);

		x2 = LOWORD(lParam);
		y2 = HIWORD(lParam);
		MoveToEx(dc, x1, y1, NULL);
		LineTo(dc, x2, y2);
		ReleaseDC(hWnd, dc);
	}
	if (type == 1)
	{

		if (!(wParam & MK_LBUTTON)) return;
		HDC dc = GetDC(hWnd);

		SetROP2(dc, R2_NOTXORPEN);
		Rectangle(dc, x1, y1, x2, y2);

		x2 = LOWORD(lParam);
		y2 = HIWORD(lParam);
		Rectangle(dc, x1, y1, x2, y2);
		ReleaseDC(hWnd, dc);
	}
	if (type == 2)
	{

		if (!(wParam & MK_LBUTTON)) return;
		HDC dc = GetDC(hWnd);

		SetROP2(dc, R2_NOTXORPEN);
		Ellipse(dc, x1, y1, x2, y2);

		x2 = LOWORD(lParam);
		y2 = HIWORD(lParam);
		Ellipse(dc, x1, y1, x2, y2);
		ReleaseDC(hWnd, dc);
	}
}